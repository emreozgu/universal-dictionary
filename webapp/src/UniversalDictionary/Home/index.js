import NavBar from "../../UI/NavBar";
import React from 'react';

class Home extends React.Component {
    render() {
        return (
            <div className="homepage">
                <NavBar/>
            </div>
        );
    }
}

export default Home;
