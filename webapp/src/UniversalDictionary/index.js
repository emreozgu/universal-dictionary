import React, { useState } from 'react';
import UDRouter from "./UDRouter";
import {UserContext} from "../Context/UserContext";

const UniversalDictionary = () => {
    const [user, setUser] = useState(null);
    return (
        <UserContext.Provider value={{user, setUser, name: "context"}}>
            <UDRouter/>
        </UserContext.Provider>
    )
};

export default UniversalDictionary;