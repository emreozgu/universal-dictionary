import React from 'react';
import Login from "./Login";
import {UserContext} from "../../Context/UserContext";

class LoginFlow extends React.Component {
    static contextType = UserContext;
    constructor(props) {
        super(props);
        this.state = { user: { username: "" }, language: "", stage: 0};
        this.updateStage = this.updateStage.bind(this);
    }

    updateStage(name, val) {
        const { stage } = this.state;
        const { setUser } = this.context;
        if (name === 'user') {
            setUser(val);
        } else {
            this.setState({stage: stage + 1, [name]: val});
        }
    }

    render() {
        const { stage } = this.state;
        if (stage === 0) {
            return <Login onClick={(val) => this.updateStage('user', val)}/>
        }
    }
}

export default LoginFlow;