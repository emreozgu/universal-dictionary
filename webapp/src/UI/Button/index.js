import React from "react";

const Button = ({text, onClick, disabled}) => (
        <div>
            <button disabled={disabled}
                    onClick={onClick}>
                {text}
            </button>
        </div>);

Button.defaultProps = {
    disabled: false,
    text: 'Click Me!'
};
export default Button;