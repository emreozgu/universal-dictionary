import React, {useRef, useEffect} from 'react';
import './index.css';

const DropDown = ({ onClose }) => {
    const window = useRef();
    const outsideClick = e => {
        if (!window.current.contains(e.target)) {
            onClose();
        }
    };

    useEffect(() => {
        document.addEventListener("mousedown", outsideClick);
        return () => document.removeEventListener("mousedown", outsideClick);
    }, []);

    // This should be populated from a back-end when we have it setup.
    return (
        <div className="dropdown" ref={window}>
            <ul><li>
                    Turkish
                </li>
                <li>
                    English
                </li>
                <li>
                    German
                </li>
            </ul>
        </div>
    )
};

export default DropDown;