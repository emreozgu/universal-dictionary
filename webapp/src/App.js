import React from 'react';
import './App.css';
import UniversalDictionary from "./UniversalDictionary";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <UniversalDictionary/>
      </header>
    </div>
  );
}

export default App;
